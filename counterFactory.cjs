
function counterFactory() {
    let count = 2;

    return  obj= {
        increment:function increment(){
            return ++count;
        }, 
        decrement:function decrement(){
            return --count;
        },
    }
    
}

module.exports = counterFactory;